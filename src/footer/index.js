import React from 'react';

import './style.css';

const Footer = () => {
  return (
    <footer>
      <div className="wrapper -box">
        Hecho por{' '}
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://twitter.com/afk_mario"
        >
          @afk_mario
        </a>{' '}
        -{' '}
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://gitlab.com/afk_mario/factura"
        >
          ¿issues?
        </a>
      </div>
    </footer>
  );
};

export default Footer;
