import React, { useState, useRef } from 'react';
import { useKeyPress } from '../utils';

// import 'rc-input-number/assets/index.css';
import './variables.css';
import './style.css';

import { MODES } from '../utils';
import Quiero from '../quiero';
import Tengo from '../tengo';

import Header from '../header';
import Footer from '../footer';
import Form from '../form';

const App = () => {
  const [mode, setMode] = useState(MODES[0]);
  const [value, setValue] = useState(1000);
  const inputEl = useRef(null);
  const quiero = mode === MODES[0];
  const nMode = quiero ? MODES[1] : MODES[0];

  useKeyPress({
    targetKey: ' ',
    onUp: () => {
      setMode(nMode);
      inputEl.current.focus();
    },
  });

  const getValues = mode === MODES[0] ? Quiero : Tengo;

  return (
    <div className="app">
      <Header mode={mode} setMode={setMode} inputEl={inputEl} />
      <Form
        value={value}
        setValue={setValue}
        getValues={getValues}
        inputEl={inputEl}
      />
      <Footer />
    </div>
  );
};

export default App;
